<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->id();
            $table->string('number')->nullable();
            $table->text('qr')->nullable();
            $table->dateTime('register_date')->nullable();
            $table->tinyInteger('status')->default(0)->nullable();
            $table->dateTime('is_visit')->nullable();
            $table->unsignedBigInteger('user_id')->nullable()->unique();
            $table->unsignedBigInteger('rb_ticket_type_id')->nullable();
            $table->timestamps();
        });

        Schema::table('tickets', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->noActionOnDelete()->onUpdate('cascade');
            $table->foreign('rb_ticket_type_id')->references('id')->on('rb_ticket_types')->noActionOnDelete()->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tickets');
    }
};
