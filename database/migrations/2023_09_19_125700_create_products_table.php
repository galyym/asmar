<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up():void
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('full_name')->comment("ФИО отправителя");
            $table->string('phone_number')->comment("Номер телефона");
            $table->string('product_name')->comment("Название продукта");
            $table->integer('count')->comment("Количество");
            $table->integer('price')->comment("Цена");
            $table->unsignedBigInteger('delivery_method_id')->comment("Метод доставки");
            $table->unsignedBigInteger('rb_region_id');
            $table->unsignedBigInteger('user_id');
            $table->timestamps();
        });

        Schema::table('products', function (Blueprint $table) {
            $table->foreign('rb_region_id')->references('id')->on('rb_regions')->noActionOnDelete()->onUpdate('cascade');
            $table->foreign('delivery_method_id')->references('id')->on('rb_deliveries')->noActionOnDelete()->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
};
