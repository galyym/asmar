<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('rb_regions', function (Blueprint $table) {
            $table->id();
            $table->string("name_kk")->nullable();
            $table->string("name_ru")->nullable();
            $table->string("name_en")->nullable();
            $table->string("description_kk")->nullable();
            $table->string("description_ru")->nullable();
            $table->string("description_en")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('rb_regions');
    }
};
