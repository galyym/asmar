<?php

namespace Database\Seeders;

use App\Models\RbTicketType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TicketTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        RbTicketType::create([
            "name_kk" => "Tiketon",
            "name_ru" => "Tiketon",
            "name_en" => "Tiketon",
            "description_kk" => "",
            "description_ru" => "",
            "description_en" => "",
        ]);

        RbTicketType::create([
            "name_kk" => "VIP",
            "name_ru" => "VIP",
            "name_en" => "VIP",
            "description_kk" => "",
            "description_ru" => "",
            "description_en" => "",
        ]);

        RbTicketType::create([
            "name_kk" => '"Іскер әйелдер кеңесінің"мүшесі',
            "name_ru" => "Член 'Совета деловых женщин'",
            "name_en" => 'Member of the "Council of Business Women"',
            "description_kk" => "",
            "description_ru" => "",
            "description_en" => "",
        ]);
    }
}
