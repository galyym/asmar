<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::create([
            'full_name' => "Паленше Паленше Паленшеулы",
            'phone_number' => "7001020300",
            'email' => "example@mail.ru",
            'birthdate' => "1999-01-01",
            'rb_region_id' => 1,
        ]);

        User::create([
            'full_name' => "Жасик",
            'phone_number' => "7071873010",
            'email' => "zhasik@mail.ru",
            'birthdate' => "2000-01-01",
            'rb_region_id' => 1,
        ]);

        User::create([
            'full_name' => "Айбат",
            'phone_number' => "7781824236",
            'email' => "aibat@mail.ru",
            'birthdate' => "2004-01-01",
            'rb_region_id' => 1,
        ]);
    }
}
