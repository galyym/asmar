<?php

namespace Database\Seeders;

use App\Models\Delivery;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DeliverySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Delivery::create([
            "name_kk" => "post.kz",
            "name_ru" => "post.kz",
            "name_en" => "post.kz",
            "description_kk" => "post.kz арқылы жеткізу",
            "description_ru" => "Доставка через post.kz",
            "description_en" => "Delivery by post.kz",
        ]);

        Delivery::create([
            "name_kk" => "inDriver",
            "name_ru" => "inDriver",
            "name_en" => "inDriver",
            "description_kk" => "inDriver арқылы жеткізу",
            "description_ru" => "Доставка через inDriver",
            "description_en" => "Delivery by inDriver",
        ]);
    }
}
