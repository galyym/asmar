<?php

return [
    "qr_generated" => "QR code has been created",
    "qr_not_generated" => "QR code was not created",
    "qr_already_generated" => " QR code has already been created",
    "not_found" =>  "Ticket was not found",
    "success" => "Ticket successfully received",
    "error" => "The ticket was not found or you have already used this ticket once."
];
