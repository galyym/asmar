<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'registration_success' => 'The user has been successfully registered',
    'registration_error' => 'Error during user registration. Check the details or you already have a ticket.',
    'user_not_found' => 'User not found',
    'verification_code_sent' => 'Confirmation code sent',
    'invalid_verify_code' =>  'The confirmation code is incorrect',
    'not_registered' => 'User not registered'
];
