<?php

return [
    "qr_generated" => "QR код был создан",
    "qr_not_generated" => "QR код не был создан",
    "qr_already_generated" => "QR код уже был создан",
    "not_found" =>  "Билет не найден",
    "success" => "Билет успешно получен",
    "error" => "Билет не найден",
    "passed" => "Вы уже один раз воспользовались этим билетом."
];
