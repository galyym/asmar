<?php

return [
    "qr_generated" => "QR коды құрылды",
    "qr_not_generated" => "QR коды құрылмады",
    "qr_already_generated" => "QR код құрылған",
    "not_found" =>  "Билет табылмады",
    "success" => "Билет сәтті алынды",
    "error" => "Билет табылмады немесе сіз бұл билетті бір рет пайдаландыңыз"
];
