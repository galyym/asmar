# ASMAR project ##
**Проект, который использует только женщины !!!**

## Пуско-наладочный процесс
Настроить в корневой папке домена .htaccess чтобы перенаправлял на нужную папку

текст файла

```
<IfModule mod_rewrite.c>
    <IfModule mod_negotiation.c>
        Options -MultiViews -Indexes
    </IfModule>

    RewriteEngine On

    # Handle Authorization Header
    RewriteCond %{HTTP:Authorization} .
    RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]

    # Redirect Trailing Slashes If Not A Folder...
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteCond %{REQUEST_URI} (.+)/$
    RewriteRule ^ %1 [L,R=301]

    # Send Requests To Front Controller...
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteRule ^ asmar_back/public/index.php [L]
</IfModule>
```
## Composer 
composer install - но с этой командой возникли проблемы потэтому запустили команду

composer install --ignore-platform-reqs

## Команды запуска

Команды которые запускали при запуске проекта
```
    cd public_html/asmar_back/
    php -v
    composer install
    ls
    composer install --ignore-platform-reqs
    php artisan optimize
    php artisan optimize:clear
    php artisan key:generate
    php artisan migrate
    php artisan migrate:fresh --seed
    php artisan db:seed
```

## Команды которые надо запускать при обновлении кода 
их запускает пайплан
   ```
   php artisan optimize
   php artisan optimize:clear
   php artisan key:generate
   php artisan migrate --seed
```






