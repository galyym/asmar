<?php

namespace App\Listeners;

use App\Events\SendWhatsAppImageEvent;
use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Http;

class SendWhatsAppImageListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\SendWhatsAppImageEvent  $event
     * @return void
     */
    public function handle(SendWhatsAppImageEvent $event)
    {
        $data = [
            "session" => "default",
            "chatId" => "7".$event->phoneNumber."@c.us",
            "file" => [
                "mimetype" => "image/svg+xml",
                "filename" => "filename.svg",
                "data" => $event->message
            ],
            "caption" => "string"
        ];

        if (Http::post("http://".config("whatsapp.ip").":".config("whatsapp.port")."/api/sendImage", $data)){
            User::updateIsSendMessageByPhoneNumber($event->phoneNumber);
        }
    }
}
