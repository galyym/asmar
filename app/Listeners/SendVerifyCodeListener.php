<?php

namespace App\Listeners;

use App\Events\SendVerifyCodeEvent;
use Illuminate\Support\Facades\Http;

class SendVerifyCodeListener
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     * @param SendVerifyCodeEvent $event
     * @return void
     */
    public function handle(SendVerifyCodeEvent $event)
    {
        $data = [
            "chatId" => "7".$event->phoneNumber."@c.us",
            "mentions" => [
                "7".$event->phoneNumber."@c.us"
            ],
            "text" => "Ваш код: " . $event->verifyCode,
            "session" => "default"
        ];

        $sendCode = Http::post("http://".config("whatsapp.ip").":".config("whatsapp.port")."/api/sendText", $data);
    }
}
