<?php

namespace App\Listeners;

use App\Events\SendMessageByWhatsAppEvent;
use App\Models\User;
use Illuminate\Support\Facades\Http;

class SendMessageByWhatsAppListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\SendMessageByWhatsAppEvent  $event
     * @return void
     */
    public function handle(SendMessageByWhatsAppEvent $event)
    {
        $data = [
            "chatId" => "7".$event->phoneNumber."@c.us",
            "mentions" => [
                "7".$event->phoneNumber."@c.us"
            ],
            "text" => $event->message,
            "session" => "default"
        ];

        if (Http::post("http://".config("whatsapp.ip").":".config("whatsapp.port")."/api/sendText", $data)){
            User::updateIsSendMessageByPhoneNumber($event->phoneNumber);
        }
    }
}
