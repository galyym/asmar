<?php

namespace App\Imports;

use App\Models\Ticket;
use Maatwebsite\Excel\Concerns\ToModel;

class TicketonTicketImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new Ticket([
            "number" => $row[0],
            "qr" => $row[0],
            "register_date" => now(),
            "status" => 5,
            "rb_ticket_type_id" => 1,
        ]);
    }
}
