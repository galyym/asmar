<?php

namespace App\Imports;

use App\Models\RbRegion;
use Maatwebsite\Excel\Concerns\ToModel;

class RegionImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new RbRegion([
            "name_kk" => $row[0],
            "name_ru" => $row[1],
            "name_en" => $row[2],
            "description_kk" => $row[3],
            "description_ru" => $row[4],
            "description_en" => $row[5]
        ]);
    }
}
