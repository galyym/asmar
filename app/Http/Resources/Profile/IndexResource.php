<?php

namespace App\Http\Resources\Profile;

use App\Http\Resources\Reference\RegionResource;
use Illuminate\Http\Resources\Json\JsonResource;

class IndexResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "full_name" => $this->full_name,
            "phone_number" => $this->phone_number,
            "region" => new  RegionResource($this->region),
            "ticket" => new \App\Http\Resources\Ticket\IndexResource($this->ticket)
        ];
    }
}
