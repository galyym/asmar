<?php

namespace App\Http\Resources\Ticket;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Hash;

class IndexResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request): array
    {
        return [
            "number" => $this->number,
            "qr" => $this->qr,
            "rb_ticket_type_id" => $this->rb_ticket_type_id,
            "register_date" => $this->register_date,
            "user" => $this->user,
            "ticket_type" => new TicketTypeResource($this->ticketType),
        ];
    }
}
