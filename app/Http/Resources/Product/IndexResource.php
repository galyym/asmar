<?php

namespace App\Http\Resources\Product;

use App\Http\Resources\Reference\DeliveryResource;
use App\Http\Resources\Reference\RegionResource;
use Illuminate\Http\Resources\Json\JsonResource;

class IndexResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "full_name" => $this->full_name,
            "phone_number" => $this->phone_number,
            "product_name" => $this->product_name,
            "count" => $this->count,
            "price" => $this->price,
            "delivery_method" => new DeliveryResource($this->deliveryMethod),
            "region" => new RegionResource($this->region)
        ];
    }
}
