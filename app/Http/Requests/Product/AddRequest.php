<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class AddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'items.*' => [
                "full_name" => "required|string",
                "rb_region_id" => "required|integer|exists:rb_regions,id",
                "phone_number" => "required|string",
                "product_name" => "required|string",
                "count" => "required|integer",
                "price" => "required|integer",
                "delivery_method_id" => "required|integer",
                'items' => 'required|array'
            ]
        ];
    }
}
