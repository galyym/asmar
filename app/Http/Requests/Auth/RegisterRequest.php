<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            "full_name" => "required|string",
            "phone_number" => "required|string|max:10|exists:users,phone_number",
            "rb_region_id" => "required|exists:rb_regions,id",
            "rb_ticket_type_id" => "nullable|exists:rb_ticket_types,id",
            "number" => "nullable|string",
        ];
    }
}
