<?php

namespace App\Http\Controllers;

use App\Services\Profile\IndexProfileService;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function profile(IndexProfileService $service){
        return $service->handle();
    }
}
