<?php

namespace App\Http\Controllers;

use App\Http\Resources\Reference\DeliveryResource;
use App\Http\Resources\Reference\RegionResource;
use App\Http\Resources\Reference\TicketTypeResource;
use App\Models\Delivery;
use App\Models\RbRegion;
use App\Models\RbTicketType;
use Illuminate\Http\Request;

class ReferenceController extends Controller
{
    public function region(){
        return RegionResource::collection(RbRegion::all());
    }

    public function status(){
        return TicketTypeResource::collection(RbTicketType::all());
    }

    public function delivery(){
        return DeliveryResource::collection(Delivery::all());
    }
}
