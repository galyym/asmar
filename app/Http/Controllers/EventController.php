<?php

namespace App\Http\Controllers;

use App\Services\Event\ShowEventService;
use Illuminate\Http\Request;

class EventController extends Controller
{
    public function show(ShowEventService $service){
        return $service->handle();
    }
}
