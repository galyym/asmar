<?php

namespace App\Http\Controllers;

use App\Http\Requests\Ticket\CheckRequest;
use App\Http\Requests\Ticket\SearchRequest;
use App\Http\Responder\Responder;
use App\Models\Ticket;
use App\Services\Ticket\CheckTicketService;
use App\Services\Ticket\SearchService;
use App\Services\Ticket\StatisticService;
use App\Models\User;

class TicketController extends Controller
{
    public function check(CheckTicketService $service, CheckRequest $request){
        return $service->handle($request->validated());
    }

    public function statistic(StatisticService $service){
        return $service->handle();
    }

    public function search(SearchService $service, SearchRequest $request, $lang, $statusId){
        return $service->handle($request->validated(), $statusId);
    }

    public function visited($lang, $userId, Responder $responder){
        $user = User::getUserById($userId);
        if ($user->status == 2){
            if (Ticket::setVisitedForVip($userId)){
                return $responder->success("Success", User::getUserById($userId));
            }
            return $responder->error("Ошибка", []);
        } else if ($user->status == 1){
            if (Ticket::setVisitedForUser($userId)){
                return $responder->success("Success", User::getUserById($userId));
            }
        }

        return $responder->error("Ошибка", []);
    }
}
