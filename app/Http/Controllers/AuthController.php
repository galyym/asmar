<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Requests\Auth\VerifyCodeRequest;
use App\Services\Auth\LoginService;
use App\Services\Auth\RegistrationService;
use App\Services\Auth\VerifyService;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(RegisterRequest $request, RegistrationService $service){
        return $service->handle($request->validated());
    }

    public function login(LoginService $service, LoginRequest $request){
        return $service->handle($request->validated());
    }

    public function verification(VerifyService $service, VerifyCodeRequest $request){
        return $service->handle($request->validated());
    }
}
