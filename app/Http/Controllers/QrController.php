<?php

namespace App\Http\Controllers;

use App\Services\Ticket\IndexService;
use App\Services\Ticket\QrCodeService;
use Illuminate\Http\Request;

class QrController extends Controller
{
    public function index(IndexService $service, Request $request){
        return $service->handle();
    }

    public function generate(QrCodeService $service, Request $request)
    {
        return $service->handle();
    }
}
