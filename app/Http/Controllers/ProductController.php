<?php

namespace App\Http\Controllers;

use App\Http\Requests\Product\AddRequest;
use App\Services\Product\AddProductService;
use App\Services\Product\IndexProductService;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function add(AddProductService $service, Request $request){
        return $service->handle($request->toArray());
    }

    public function index(IndexProductService $service){
        return $service->handle();
    }
}
