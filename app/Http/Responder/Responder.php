<?php

namespace App\Http\Responder;

use Illuminate\Http\JsonResponse;

class Responder
{
    public function __construct()
    {
    }

    /**
     * @param $success
     * @param $message
     * @param array $data
     * @param $statusCode
     * @return JsonResponse
     */
    private function respond($success, $message, $data = [], $statusCode)
    {
        return response()->json([
            "status" => $success,
            "message" => $message,
            "data" => $data
        ], $statusCode);
    }

    public function success($message = null, $data = [], $statusCode = 200): JsonResponse
    {
        return $this->respond(true, $message, $data, $statusCode);
    }

    public function error($message, $data = [], $statusCode = 400): JsonResponse
    {
        return $this->respond(false, $message, $data, $statusCode);
    }
}
