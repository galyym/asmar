<?php

namespace App\Traits;

use Revolution\Google\Sheets\Facades\Sheets;

trait HasConsoleCommand
{
    public function connectSheet(string $docKey, string $sheetName){
        return Sheets::spreadsheet($docKey)
            ->sheet($sheetName)
            ->get();
    }
}
