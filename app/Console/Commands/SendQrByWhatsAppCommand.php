<?php

namespace App\Console\Commands;

use App\Events\SendMessageByWhatsAppEvent;
use App\Events\SendWhatsAppImageEvent;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class SendQrByWhatsAppCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send-training:whatsapp';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Отправим сообщение в WhatsApp';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $confirm = $this->ask('WhatsApp арқылы рассылка жасалады, жасап жатқан іс-әрекетің дұрыс екеніне сенімдісің ба? (y/n)');
        if ($confirm != 'y') {
            dd("Stopped");
        }
        $this->info('Starting...');
        $users = User::getUserByStatus(1);
        $pauseCount = 0;
        foreach ($users as $user) {
            $ticket = $user->ticket;
            $qrCode = QrCode::size(1000)->generate($ticket->qr, storage_path("qr/$user->phone_number$ticket->number.svg"));
            if ($pauseCount%20 == 0){
                $confirm = $this->ask('Барлығы дұрыс па? (y/n)');
                if ($confirm == 'n'){
                    dd("STOPPED");
                }
            }
            $pauseCount++;
        }
        $this->info('Success!');
        return Command::SUCCESS;
    }
}
