<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Traits\HasConsoleCommand;
use Google\Service\Sheets\Sheet;
use Illuminate\Console\Command;

class ImportTrainingUser extends Command
{
    use HasConsoleCommand;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'training-user:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Импортировать пользователей тренинга';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->comment("started import users...");
        $sheet = $this->getDataFromSheet("Sheet1");
        $phoneNumbers = $this->getPhoneNumber($sheet);
        User::addUsersWithTicket($phoneNumbers);
        $this->info("success imported users");
        return Command::SUCCESS;
    }

    public function getDataFromSheet(string $sheetName){
        $documentKey = "1DOJOqdKS8EhW8McTL97gnv9ehZzPq1774rjZAPzrNk8";
        $sheets = $this->connectSheet($documentKey, $sheetName);
        $data = $sheets->all();
        $users = [];
        foreach ($data as $value) {
            if (count($value) == 9 && trim($value[0]) != ""){
                $users[] = $value;
            }
        }
        return $users;
    }

    public function getPhoneNumber(array $users){
        $phoneNumbers = [];
        foreach ($users as $u){
            if ($u[8] != null || trim($u[8]) != ""){
                $phone = trim($u[8]);
                if (strlen($phone) == 11){
                    $phoneNumbers[] = [
                        "full_name" => $u[1],
                        "phone_number" => substr($phone, -10)
                    ];
                } elseif (strlen($phone) == 10){
                    $phoneNumbers[] = [
                        "full_name" => $u[1],
                        "phone_number" => $phone
                    ];
                } else if(strlen($phone) > 20){
                    $phone = explode(',', $phone);
                    $phoneNumbers[] = [
                        "full_name" => $u[1],
                        "phone_number" => substr($phone[0], -10)
                    ];
                } else {
                    $phoneNumbers[] = [
                        "full_name" => $u[1],
                        "phone_number" => substr($phone, -10)
                    ];
                }
            }
        }
        return $phoneNumbers;
    }
}
