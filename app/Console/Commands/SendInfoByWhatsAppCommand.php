<?php

namespace App\Console\Commands;

use App\Events\SendMessageByWhatsAppEvent;
use App\Models\User;
use Illuminate\Console\Command;

class SendInfoByWhatsAppCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:whatsapp';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Отправим сообщение в WhatsApp';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $confirm = $this->ask('WhatsApp арқылы рассылка жасалады, жасап жатқан іс-әрекетің дұрыс екеніне сенімдісің ба? (y/n)');
        if ($confirm != 'y') {
            dd("Stopped");
        }
        $this->info('Starting...');
        $users = User::getUserByStatus(1);
        $pauseCount = 0;
        foreach ($users as $user) {
            event(new SendMessageByWhatsAppEvent($user->phone_number, "Сіз 27.09.2023 ж. Ақтау қаласында өтетін ӘЙЕЛДЕР КӘСІПКЕРЛІГІНІҢ ХАЛЫҚАРАЛЫҚ КОНГРЕСІ “ASMAR – БІРЛІК ДӘСТҮРЛЕРІ” іс – шарасына қатысуға мүмкіндік беретін QR- кодты осы сілтеме арқылы ала аласыз: https://asmar.process.kz/

Вы можете получить QR-код для участия в МЕЖДУНАРОДНОМ КОНГРЕССЕ ЖЕНСКОГО ПРЕДПРИНИМАТЕЛЬСТВА “ASMAR – ТРАДИЦИИ ЕДИНСТВА”, которое состоится 27.09.2023 г. в городе Актау, перейдя по этой ссылке: https://asmar.process.kz/

Сілтемені ашу үшін, осы номерді өзіңізге сақтап алуыңыз керек.
Чтобы открыть ссылку, следует сохранить в контакты данный номер."));
            if ($pauseCount%20 == 0){
                sleep(5);
                $confirm = $this->ask('Барлығы дұрыс па? (y/n)');
                if ($confirm == 'n'){
                    dd("STOPPED");
                }
            }
            $pauseCount++;
        }
        $this->info('Success!');
        return Command::SUCCESS;
    }
}
