<?php

namespace App\Console\Commands;

use App\Traits\HasConsoleCommand;
use App\Models\User;
use Illuminate\Console\Command;
use Revolution\Google\Sheets\Facades\Sheets;

class ImportUser extends Command
{
    use HasConsoleCommand;

    const ALL_SHEET_NAME = [
        "г. Астана", "г. Алматы", "г. Шымкент", "Абайская область", "Акмолинская область", "Атырауская область", "Актюбинская область",
        "Алматинская область", "ВКО", "Жамбылская область", "Жетысуская область", "Карагандинская область", "ЗКО", "Кызылординская область",
        "СКО", "Костанайская область", "Мангистауская область", "Павлодарская область", "Туркестанская область", "Улытауская область"
    ];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Импортируем пользователей из Google Sheet';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Importing user...');

        $users = [];
        foreach (self::ALL_SHEET_NAME as $sheetName) {
            $user = $this->getDataFromSheet($sheetName);
            $users[] = $user;
        }

        $phoneNumbers = $this->getPhoneNumber($users);
        User::userUpdateByName($phoneNumbers);

        $this->info('User imported');
        return Command::SUCCESS;
    }

    public function getDataFromSheet(string $sheetName){
        $documentKey = config("google.document_key");
        $sheets = $this->connectSheet($documentKey, $sheetName);
        $data = $sheets->all();
        $users = [];
        foreach ($data as $value) {
            if (count($value) == 9 && trim($value[0]) != ""){
                $users[] = $value;
            }
        }
        return $users;
    }

    public function getPhoneNumber(array $users){
        $phoneNumbers = [];
        foreach ($users as $user) {
            foreach ($user as $u){
                if ($u[8] != null || trim($u[8]) != ""){
                    $phone = trim($u[8]);
                    if (strlen($phone) == 11){
                        $phoneNumbers[] = [
                            "full_name" => $u[1],
                            "phone_number" => substr($phone, -10)
                        ];
                    } elseif (strlen($phone) == 10){
                        $phoneNumbers[] = [
                            "full_name" => $u[1],
                            "phone_number" => $phone
                        ];
                    } else if(strlen($phone) > 20){
                        $phone = explode(',', $phone);
                        $phoneNumbers[] = [
                            "full_name" => $u[1],
                            "phone_number" => substr($phone[0], -10)
                        ];
                    } else {
                        $phoneNumbers[] = [
                            "full_name" => $u[1],
                            "phone_number" => substr($phone, -10)
                        ];
                    }
                }
            }
        }
        return $phoneNumbers;
    }
}
