<?php

namespace App\Console\Commands;

use App\Imports\RegionImport;
use App\Imports\TicketonTicketImport;
use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;

class ImportTicketonTicketCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ticketon:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Импортируем билеты из Ticketon';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('start import from Ticketon');
        Excel::import(new TicketonTicketImport, storage_path("excelUser/Отчёт по билетам от 2023-09-26_14-03-18.xlsx"));
        return Command::SUCCESS;
    }
}
