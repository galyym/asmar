<?php

namespace App\Console\Commands;

use App\Imports\RegionImport;
use App\Imports\UsersImport;
use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;

class ImportRegionCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'region:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Importing user...');
        Excel::import(new RegionImport, storage_path("excelUser/regions.xlsx"));
        $this->info('User imported');
        return Command::SUCCESS;
    }
}
