<?php

namespace App\Console\Commands;

use App\Traits\HasConsoleCommand;
use App\Models\User;
use Illuminate\Console\Command;
use Revolution\Google\Sheets\Facades\Sheets;

class ImportVIPCommand extends Command
{
    use HasConsoleCommand;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vip:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Импортируем VIP гостей';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info("Starting import...");
        $importUser = new ImportUser();
        $connect = $this->connectSheet(config('google.document_key'), "ВИП");
        $users = $connect->all();

        foreach ($users as $user) {
            if ($user[0] != ""){
                User::addVIP($user);
            }
        }

        $this->info("Import finished.");
        return Command::SUCCESS;
    }
}
