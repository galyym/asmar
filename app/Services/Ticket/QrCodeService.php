<?php

namespace App\Services\Ticket;

use App\Models\Ticket;
use App\Services\BaseService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class QrCodeService extends BaseService
{
    public function handle(){
        $user = Auth::user();
        if ($user->ticket) return $this->responder->error(__("ticket.qr_already_generated"));
        if (Ticket::createTicket($user->id)){
            return $this->responder->success(__("ticket.qr_generated"));
        }
        return $this->responder->error(__("ticket.qr_not_generated"));
    }
}
