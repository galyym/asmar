<?php

namespace App\Services\Ticket;

use App\Models\Ticket;
use App\Services\BaseService;
use Illuminate\Http\JsonResponse;

class CheckTicketService extends BaseService
{
    public function handle(array $data) : JsonResponse {
        $qr = Ticket::getQr($data["qr"]);
        if(!$qr) return $this->responder->error(__("ticket.error"), [], 404);
        elseif (Ticket::checkVisitDate($qr->id)) return $this->responder->error(__("ticket.passed"), [], 400);
        Ticket::setIsVisit($qr->id);
        return $this->responder->success(__("ticket.success"), $qr->user);
    }
}
