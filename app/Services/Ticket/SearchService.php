<?php

namespace App\Services\Ticket;

use App\Models\User;
use App\Services\BaseService;

class SearchService extends BaseService
{
    public function handle(array $data, $statusId){
        $user = User::searchUserByUserNameAndStatus($statusId, array_key_exists("name", $data) ? $data["name"] : null);
        return $this->responder->success(__("Success"), $user);
    }
}
