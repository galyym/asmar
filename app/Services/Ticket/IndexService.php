<?php

namespace App\Services\Ticket;

use App\Http\Resources\Ticket\IndexResource;
use App\Services\BaseService;
use Illuminate\Support\Facades\Auth;

class IndexService extends BaseService
{
    public function handle(){
        $user = Auth::user();
        if(!$user->ticket) return $this->responder->error(__("ticket.not_found"));
        return $this->responder->success(__("ticket.success"), new IndexResource($user->ticket));
    }
}
