<?php

namespace App\Services\Ticket;

use App\Models\Ticket;
use App\Services\BaseService;
use Illuminate\Http\JsonResponse;

class StatisticService extends BaseService
{
    /**
     * @return JsonResponse
     */
    public function handle(): JsonResponse
    {
        $activeTickets = Ticket::getActiveTicket();
        $inActiveTickets = Ticket::getInActiveTicket();
        return $this->responder->success(__("ticket.success"), [
            "active" => $activeTickets,
            "inActive" => $inActiveTickets,
        ]);
    }
}
