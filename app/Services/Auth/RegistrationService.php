<?php

namespace App\Services\Auth;

use App\Models\Ticket;
use App\Models\User;
use App\Services\BaseService;

class RegistrationService extends BaseService
{
    public function handle(array $data){
        $addUser = User::userUpdateByPhoneNumber($data);
        $ticket = null;
        if ($addUser && (array_key_exists("rb_ticket_type_id", $data) || array_key_exists("number", $data))){
            $user = User::getByPhoneNumber($data['phone_number']);
            $ticket = $user
                ? Ticket::createTicket($user->id, $data["rb_ticket_type_id"] ?? null, $data["number"] ?? null)
                : null;
        }
        return $ticket
            ? $this->responder->success(__("auth.registration_success"), $user)
            : $this->responder->error(__("auth.registration_error"));
    }
}
