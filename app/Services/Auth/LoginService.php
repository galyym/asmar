<?php

namespace App\Services\Auth;

use App\Events\SendVerifyCodeEvent;
use App\Services\BaseService;
use App\Models\User;
use Illuminate\Support\Facades\Redis;

class LoginService extends BaseService
{
    public function handle(array $data){
        $user = User::checkUserByPhoneNumber($data["phone_number"]);
        if (!$user) return $this->responder->error(__("auth.user_not_found"));
        $verificationCode = rand(1000, 9999);
        Redis::setex("verification_code:".$user->phone_number, 5000, $verificationCode);
        event(new SendVerifyCodeEvent($user->phone_number, $verificationCode));
        return $this->responder->success(__("auth.verification_code_sent"), $user);
    }
}
