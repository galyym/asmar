<?php

namespace App\Services\Auth;

use App\Models\Ticket;
use App\Services\BaseService;
use Illuminate\Support\Facades\Redis;
use App\Models\User;

class VerifyService extends BaseService
{
    public function handle(array $data){
        $verificationCode = Redis::get("verification_code:".$data["phone_number"]);

        if ($verificationCode !== $data["code"]) {
            return $this->responder->error(__('auth.invalid_verify_code'), [], 400);
        }
        $user = User::where('phone_number', $data["phone_number"])->first();
        Redis::del("verification_code:".$data["phone_number"]);
        Ticket::createTicket($user->id);

        $user->ticket->status = Ticket::TICKET_STATUS_ACTIVE;
        $user->ticket->save();

        if ($user) {
            $token =  $user->createToken('authToken')->plainTextToken;
            return [
                "token" => $token,
                "user" => $user
            ];
        }
        return ["user" => $user];
    }
}
