<?php

namespace App\Services;

use App\Http\Responder\Responder;

class BaseService
{
    public function __construct(
        protected Responder $responder
    ){}
}
