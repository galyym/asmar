<?php

namespace App\Services\Profile;

use App\Http\Resources\Profile\IndexResource;
use App\Services\BaseService;
use Illuminate\Support\Facades\Auth;

class IndexProfileService extends BaseService
{
    public function handle(){
        $user = Auth::user();
        $user = $user->load("region");
        $user = $user->load("ticket");
        return new IndexResource($user);
    }
}
