<?php

namespace App\Services\Product;

use App\Http\Resources\Product\IndexResource;
use App\Models\Product;
use App\Services\BaseService;
use Illuminate\Support\Facades\Auth;

class IndexProductService extends BaseService
{
    public function handle(){
        $user = Auth::user();
        return $this->responder->success(__("product.index"), IndexResource::collection(Product::getProduct($user->id)));
    }
}
