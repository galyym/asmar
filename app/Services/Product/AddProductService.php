<?php

namespace App\Services\Product;

use App\Models\Product;
use Illuminate\Support\Facades\Auth;

class AddProductService
{
    public function handle(array $data){
        $userId = Auth::id();
        $addProduct = Product::addProduct($data, $userId);
        return $addProduct;
    }
}
