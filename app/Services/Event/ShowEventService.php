<?php

namespace App\Services\Event;

use App\Models\ScheduleEvent;
use App\Services\BaseService;

class ShowEventService extends BaseService
{
    public function handle(){
        $events = ScheduleEvent::get()->groupBy('date');
        return $this->responder->success(__("event.list"), $events);
    }
}
