<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Str;

class Ticket extends Model
{
    use HasFactory;

    CONST TICKET_STATUS_ACTIVE = 1;
    CONST TICKET_STATUS_INACTIVE = 0;

    protected $with = ['user', 'ticketType'];

    protected $fillable = [
        'number',
        'qr',
        'user_id',
        'register_date',
        'rb_ticket_type_id',
        'status',
        'is_visit'
    ];

    public function user(): BelongsTo|null
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function ticketType(): BelongsTo|null
    {
        return $this->belongsTo(RbTicketType::class, 'rb_ticket_type_id', 'id');
    }

    public static function createTicket($userId, $ticketTypeId = null, $number = null){
        if (User::checkTicketByUserId($userId)) return false;
        $lastCardNumber = self::getLastTicketNumber();
        $cardNumber = $lastCardNumber
            ? (int)$lastCardNumber->number + 1
            : 200000;

        $qrHash = Str::random(128);
        return self::create([
            'number' => $number ?? $cardNumber,
            'rb_ticket_type_id' => $ticketTypeId ?? null,
            'qr' => $qrHash,
            'user_id' => $userId,
            'register_date' => now(),
            'status' => self::TICKET_STATUS_INACTIVE
        ]);
    }

    public static function getLastTicketNumber(){
        return self::orderBy('number', 'desc')
            ->first();
    }

    public static function getQr(string $qr): Ticket|null
    {
        return self::where("qr", $qr)
            ->first();
    }

    public static function setIsVisit(int $id){
        return self::find($id)
            ->update([
                "is_visit" => now()
            ]);
    }

    public static function getActiveTicket(): int
    {
        return self::whereNull("is_visit")
            ->count();
    }

    public static function getInActiveTicket(): int
    {
        return self::whereNotNull("is_visit")
            ->count();
    }

    public static function checkVisitDate($id){
        return self::find($id)->is_visit;
    }

    public static function setVisitedForVip($userId){
        return self::updateOrCreate(["user_id" => $userId],[
                "is_visit" => now(),
                "user_id" => $userId
            ]);
    }

    public static function setVisitedForUser($userId){
        return self::where("user_id", $userId)
            ->update([
                "is_visit" => now()
            ]);
    }
}
