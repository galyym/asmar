<?php

namespace App\Models;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @mixin Builder
 */
class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        "full_name",
        "rb_region_id",
        "phone_number",
        "product_name",
        "count",
        "price",
        "delivery_method_id",
        "user_id"
    ];

    public function deliveryMethod(){
        return $this->belongsTo(Delivery::class,  "delivery_method_id", "id");
    }

    public function region(){
        return $this->belongsTo(RbRegion::class,  "rb_region_id", "id");
    }

    public static function addProduct(array $data, $userId)
    {
            foreach ($data["items"] as $key => $value){
                $value["user_id"] = $userId;
                self::create($value);
            }
            return true;
    }

    public static function getProduct(int $userId){
        return self::where("user_id", $userId)
            ->with(["deliveryMethod", "region"])
            ->get();
    }
}
