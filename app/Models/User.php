<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Laravel\Sanctum\HasApiTokens;

/**
 * @mixin Builder
 */
class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'full_name',
        'phone_number',
        'email',
        'is_send_message',
        'status'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function ticket(){
        return $this->belongsTo(Ticket::class, 'id', 'user_id');
    }

    public function region(){
        return $this->belongsTo(RbRegion::class, 'rb_region_id', 'id');
    }

    public static function userUpdateByPhoneNumber(array $data): int
    {
        return self::upsert([
            "full_name" => $data["full_name"],
            "phone_number" => $data["phone_number"],
            "rb_region_id" => $data["rb_region_id"]
        ], ["phone_number"]);
    }

    public static function userUpdateByName(array $data): int
    {
        return self::upsert($data, ["full_name"]);
    }

    public static function checkUserByPhoneNumber($phoneNumber): User|null
    {
        return self::where("phone_number", $phoneNumber)
            ->first();
    }

    public static function getByPhoneNumber($phoneNumber){
        return self::where("phone_number", $phoneNumber)
            ->first();
    }

    public static function checkTicketByUserId($userId){
        return self::find($userId)->ticket;
    }

    public static function getUserByStatus(int $statusId){
        return self::where("status", $statusId)
            ->where("is_send_message", 1)
            ->get();
    }

    public static function searchUserByUserNameAndStatus($statusId, $userName){
        $users =  self::where("status", $statusId);

        if ($userName){
            $users = $users->where("full_name", "like", "%$userName%");
        }

        return $users->with("ticket")
            ->get();
    }

    public static function updateIsSendMessageByPhoneNumber($phoneNumber){
        return self::where("phone_number", "like", "%$phoneNumber%")
            ->update([
                "is_send_message" => 2
            ]);
    }

    public static function getUserById($id){
        return self::where("id", $id)
            ->with("ticket")
            ->first();
    }

    public static function addVIP($user){
        return self::updateOrCreate(["full_name" => $user[1]], [
            "full_name" => $user[1],
            "status" => 2,
            "is_send_message" => 2,
        ]);
    }

    public static function addUsersWithTicket($users){
        foreach ($users as $user){
            $user = self::updateOrCreate(["full_name" => $user["full_name"]], $user);
            if(!$user->ticket){
                Ticket::createTicket($user->id);
            }
        }
    }
}
