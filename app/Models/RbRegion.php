<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RbRegion extends Model
{
    use HasFactory;

    protected $fillable = [
        "name_kk",
        "name_ru",
        "name_en",
        "description_kk",
        "description_ru",
        "description_en"
    ];
}
