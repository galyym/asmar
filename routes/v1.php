<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\QrController;
use App\Http\Controllers\ReferenceController;
use App\Http\Controllers\TicketController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::group(['prefix' => '{lang}', 'where' => ['kk|ru|en']], function () {
    Route::post("register", [AuthController::class, "register"]);
    Route::get("login", [AuthController::class, "login"]);
    Route::get("verification", [AuthController::class, "verification"]);
    Route::middleware('auth:sanctum')
        ->group(function (){
            Route::prefix('ticket')
                ->as('ticket.')
                ->group(function (){
                    Route::get('',[QrController::class, 'index']);
                    Route::get('generate',[QrController::class, 'generate']);
                    Route::get('check', [TicketController::class, 'check']);
                    Route::get("statistic", [TicketController::class, "statistic"]);
                    Route::get("search/{statusId}",  [TicketController::class, "search"]);
                    Route::get("visited/{userId}", [TicketController::class, "visited"]);
                });
            Route::prefix('event')
                ->as('event.')
                ->group(function (){
                    Route::get('', [EventController::class, 'show']);
                });

            Route::prefix('profile')
                ->as('profile.')
                ->group(function (){
                    Route::get('', [ProfileController::class, 'profile']);
                });

            Route::prefix('product')
                ->as('product.')
                ->group(function (){
                    Route::post('', [ProductController::class, 'add']);
                    Route::get('', [ProductController::class, 'index']);
                });
        });

    Route::prefix('reference')
        ->group(function (){
        Route::get('region', [ReferenceController::class, 'region']);
        Route::get('status', [ReferenceController::class, 'status']);
        Route::get('delivery/method', [ReferenceController::class, 'delivery']);
    });
});
