<?php

return [
    "ip" => env("WHATSAPP_IP"),
    "port" => env("WHATSAPP_PORT")
];
